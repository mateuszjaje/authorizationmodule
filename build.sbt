import SonatypeKeys._

sonatypeSettings

organization := "pl.sigmapoint" 

profileName := "morgarothsigmapoint" 

version := "0.1" 

pomExtra := {
  <url>https://bitbucket.org/mateuszjaje/authorizationmodule</url>
  <!-- License of your choice -->
  <licenses>
    <license>
      <name>Apache 2</name>
      <url>http://www.apache.org/licenses/LICENSE-2.0.txt</url>
    </license>
  </licenses>
  <!-- SCM information. Modify the follwing URLs -->
  <scm>
    <connection>scm:git:github.com/(your repository URL)</connection>
    <developerConnection>scm:git:git@github.com:(your repository URL)</developerConnection>
    <url>github.com/(your repository url)</url>
  </scm>
  <!-- Developer contact information -->
  <developers>
    <developer>
      <id>(your favorite id)</id>
      <name>(your name)</name>
      <url>(your web page)</url>
    </developer>
  </developers>
}